import torch
import torch.nn as nn
import torch.nn.functional as F

from config import YoruConfig
from transformers import AutoModelForCausalLM

class ImagePatching(nn.Module):
    def __init__(self):
        super(ImagePatching, self).__init__()
        self.patch_size = YoruConfig.patch_size
        self.image_size = YoruConfig.image_size
        self.num_patches = (self.image_size // self.patch_size) ** 2
        self.conv = nn.Conv2d(3, YoruConfig.embedding_dim, self.patch_size, self.patch_size)

    def forward(self, x):
        x = self.conv(x)
        x = x.permute(0, 2, 3, 1)
        x = x.view(-1, self.num_patches, YoruConfig.embedding_dim)
        return x

class Yoru(nn.Module):
    def __init__(self):
        super(Yoru, self).__init__()
        self.transformer = AutoModelForCausalLM.from_pretrained(YoruConfig.llm_model_name)

        self.patching = ImagePatching()
        self.projection = nn.Linear(YoruConfig.embedding_dim, YoruConfig.vocab_size)

    def forward(self, text_ids, image):
        image = self.patching(image)
        image = self.projection(image)

        image = torch.softmax(image, dim=-1)
        image = torch.argmax(image, dim=-1)

        concatenated = torch.cat((text_ids, image), dim=-1)

        outputs = self.transformer.generate(concatenated, max_length=YoruConfig.max_new_tokens)
        
        return outputs
    
# model = Yoru()
# output = model("Hello, World!", torch.randn(1, 3, 224, 224))

# output_text = model.tokenizer.batch_decode(output)
# print(output_text)

# def count_parameters(model):
#     return sum(p.numel() for p in model.parameters() if p.requires_grad)

# print(count_parameters(model))