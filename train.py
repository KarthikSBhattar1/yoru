import torch
from torch.utils.data import Dataset
from transformers import Trainer, TrainingArguments, AutoTokenizer

from datasets import load_dataset
from PIL import Image
from model import Yoru

import requests
import numpy as np
from io import BytesIO
from config import YoruConfig
import os

class LaionDataset(Dataset):
    def __init__(self, dataset):
        self.dataset = dataset
        self.tokenizer = AutoTokenizer.from_pretrained(YoruConfig.llm_model_name)
        self.tokenizer.pad_token = self.tokenizer.eos_token

    def __len__(self):
        return len(self.dataset)

    def __getitem__(self, idx):
        item = self.dataset[idx]
        image_url = item["link"]
        caption = item["caption"]

        try:
            response = requests.get(image_url)
            image = Image.open(BytesIO(response.content)).convert("RGB")
            image = image.resize((YoruConfig.image_size, YoruConfig.image_size))
            image = torch.from_numpy(np.array(image)).permute(2, 0, 1).float() / 255.0
        except Exception as e:
            print(f"Error loading image: {e}")
            image = torch.zeros(3, YoruConfig.image_size, YoruConfig.image_size)

        caption_ids = self.tokenizer.encode(caption, add_special_tokens=False)
        caption_ids = torch.tensor(caption_ids, dtype=torch.long)

        return {
            "image": image,
            "text_ids": torch.zeros(self.tokenizer.pad_token_id, dtype=torch.long),  # Provide pad token for text input
            "labels": caption_ids,
        }

# Load the dataset
dataset = load_dataset("laion/gpt4v-dataset")
train_dataset = LaionDataset(dataset["train"])

# Define the training arguments
training_args = TrainingArguments(
    output_dir="output",
    num_train_epochs=3,
    per_device_train_batch_size=8,
    gradient_accumulation_steps=4,
    learning_rate=1e-4,
    warmup_steps=1000,
    logging_steps=100,
    save_steps=1000,
    save_total_limit=2,
)

# Create the model
model = Yoru()

# Create the trainer
trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=train_dataset,
)

# Start training
trainer.train()