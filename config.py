from dataclasses import dataclass
from transformers import AutoTokenizer

from dotenv import load_dotenv
load_dotenv()

import os

tokenizer = AutoTokenizer.from_pretrained("gpt2")

@dataclass
class YoruConfig:
    llm_model_name="gpt2"

    vocab_size=tokenizer.vocab_size
    token=os.environ["LLAMA_API_KEY"]

    patch_size=16
    image_size=224

    embedding_dim=768
    max_new_tokens=512